---
layout: page
title: Resources
permalink: /resources.html
---

If you want to help us spread the word, here are some PDFs of leaflets you can print and share.

[A full-colour UK-specific version of the GWU zine](/assets/zine-fullcolour-uk.pdf)

[A black and white tri-fold leaflet](/assets/trifold-bw-uk.pdf)
