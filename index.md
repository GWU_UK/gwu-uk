---
layout: page
title: Home
permalink: /
---

This site is currently very much under construction, please excuse the lack of content.


For more information on GWU please see [the main GWU site](https://www.gameworkersunite.org/). 


If you are here because you want to join the UK chapter, please go ahead and fill in the form on our [joining page](/join.html)!

# Why do we need a Union?
**74%** of game workers are not paid overtime, but **90%** can be expected to work it

**53%** of game workers believe that their skillset could secure better wages and conditions in another industry

**Almost a third** of game workers believe staff diversity within the industry to be &quot;Not Good&quot;

{:.SmallText}
Figures from [the 2017 GI.Biz Survey](https://www.gamesindustry.biz/articles/2017-04-03-an-industry-driven-by-passion-not-pay).